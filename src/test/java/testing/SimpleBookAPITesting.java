package testing;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.hamcrest.Matchers.*;

import static io.restassured.RestAssured.*;

public class SimpleBookAPITesting {
    private static String token;
    private static String orderId;
    private static String name = "";
    private static String updatedName = "kavin";

    @Test (priority = 1)
    public void testGetAPI() {
        baseURI = "https://simple-books-api.glitch.me";
        given()
                .get("/books")
                .then()
                .statusCode(200);
        given()
                .get("/books/5")
                .then()
                .statusCode(200);
    }

    @Test(priority = 2)
    public void testPostAPI() {
        baseURI = "https://simple-books-api.glitch.me";
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (int index = 0; index < 7; index++) {
            name += characters.charAt((int) Math.floor(Math.random() * characters.length()));
        }
        JSONObject requestTokenBody = new JSONObject();
        requestTokenBody.put("clientName", name);
        requestTokenBody.put("clientEmail", name + "@gmail.com");
        token = given()
                .header("Content-Type", "application/json")
                .body(requestTokenBody.toJSONString())
                .when()
                .post("/api-clients")
                .then()
                .statusCode(201)
                .extract()
                .path("accessToken");

        JSONObject requestOrder = new JSONObject();
        requestOrder.put("bookId", 5);
        requestOrder.put("customerName", name);
        orderId = given()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + token)
                .body(requestOrder.toJSONString())
                .when()
                .post("/orders")
                .then()
                .statusCode(201)
                .body("created", equalTo(true))
                .extract()
                .path("orderId");
    }

    @Test(priority = 3)
    public void testPatchAPI() {
        baseURI = "https://simple-books-api.glitch.me";
        JSONObject request = new JSONObject();
        request.put("customerName", updatedName);
        given()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + token)
                .body(request.toJSONString())
                .when()
                .patch("/orders/" + orderId)
                .then()
                .statusCode(204);
    }

    @Test(priority = 4)
    public void testDeleteAPI() {
        baseURI = "https://simple-books-api.glitch.me";
        JSONObject request = new JSONObject();
        request.put("customerName", updatedName);
        given()
                .header("Content-Type", "application/json")
                .header("Authorization","Bearer "+ token)
                .body(request.toJSONString())
                .when()
                .delete("orders/"+orderId)
                .then()
                .statusCode(204);
    }
}
